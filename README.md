# Wood_Theme

![thumbnail.png](./metacity-1/thumbnail.png?raw=true "Thumbnail")

## About

Wood Theme for Linux Mint Cinnamon. Cobbled together from Moonshine and Windows_XP_NT_Theme

Modified cinnamon and metacity-1 folders.
Modified gtk-3.20 folder with meatacity1 images and edited gtk.css for Mint 21 compatibility
No changes currently made to controls.
No changes made to gnome-shell, unity, xfwm4, or older / newer gtk folders content (except thumbmails).

## Setup

Place content in its own folder named 'Wood' in the .themes directory under ~ (home/username).

### Mint/Cinnamon <=20.3

Select name in Cinnamon System Settings, Themes, 1st/3rd/5th buttons.

### Mint/Cinnamon >=21.0

Select name in Cinnamon System Settings, Themes, 2nd/4th buttons.

![thumbnail.png](./cinnamon/thumbnail.png?raw=true "Screenshot")
